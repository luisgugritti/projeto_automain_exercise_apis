package apislistforpractice.apis.tests;

import apislistforpractice.base.BaseTest;
import io.restassured.response.Response;
import org.junit.Assert;

import static apislistforpractice.base.Endpoint.*;
import static apislistforpractice.base.Request.requestJsonSearchProduct;
import static apislistforpractice.base.Response.*;

public class ApisTest extends BaseTest {

    private String endpoint;
    private String request;

    public void usarEndPoint(String endPoint) {
        switch (endPoint) {
            case "Get All Products List":
            case "POST To All Products List":
                endpoint = endpointProductList;
                break;
            case "Get All Brands List":
            case "PUT To All Brands List":
                endpoint = endpointBrandsList;
                break;
            case "POST To Search Product":
                endpoint = endpointSearchProduct;
                request = requestJsonSearchProduct;
                break;
            case "DELETE To Verify Login":
                endpoint = endpointVerifyLogin;
                break;
            default:
                throw new IllegalArgumentException("Endpoint inválido: " + endPoint);
        }
    }

    public void validarStatusCodeResponse(String requestType, String api) {
        Response response;
        int expectedStatusCode;
        String expectedResponseContent;

        switch (api) {
            case "Api 1":
                expectedStatusCode = 200;
                expectedResponseContent = responseGetAllProducts;
                break;
            case "Api 2":
                expectedStatusCode = 200;
                expectedResponseContent = responsePostAllProducts;
                break;
            case "Api 3":
                expectedStatusCode = 200;
                expectedResponseContent = responseGetBrandsList;
                break;
            case "Api 4":
                expectedStatusCode = 200;
                expectedResponseContent = responsePutBrandsList;
                break;
            case "Api 5":
            case "Api 6":
                expectedStatusCode = 200;
                expectedResponseContent = responsePostSearchProduct;
                break;
            case "Api 7":
                expectedStatusCode = 200;
                expectedResponseContent = responseDeleteVerifyLogin;
                break;
            default:
                throw new IllegalArgumentException("API não reconhecida: " + api);
        }

        if ("GET".equalsIgnoreCase(requestType)) {
            response = getRequest(endpoint);
        } else if ("POST sem Body".equalsIgnoreCase(requestType)) {
            response = postRequestNoBody(endpoint);
        } else if ("PUT sem Body".equalsIgnoreCase(requestType)) {
            response = putRequestNoBody(endpoint);
        } else if ("POST".equalsIgnoreCase(requestType)) {
            response = postRequest(endpoint, request);
        } else if ("DELETE".equalsIgnoreCase(requestType)) {
            response = deleteRequest(endpoint);
        } else {
            throw new IllegalArgumentException("Tipo de requisição inválido: " + requestType);
        }

        int statusCode = getStatusCode(response);
        String jsonResponse = response.asString();

        Assert.assertEquals(expectedStatusCode, statusCode);
        Assert.assertTrue(jsonResponse.contains(expectedResponseContent));
        System.out.println("Status Code: " + statusCode);
        System.out.println("Resposta JSON: " + jsonResponse);
    }

}
