package apislistforpractice.apis.steps;

import apislistforpractice.apis.tests.ApisTest;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;

public class ApisSteps {

    ApisTest apiPage = new ApisTest();

    @Given("^usar o endpoint (.*)$")
    public void usarEndPoint(String endPoint) {
        apiPage.usarEndPoint(endPoint);
    }

    @Then("^validar API (.*) com status code e response da (.*)$")
    public void validarStatusCodeResponse(String requestType, String api) {
        apiPage.validarStatusCodeResponse(requestType, api);
    }

}
