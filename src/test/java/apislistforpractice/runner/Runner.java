package apislistforpractice.runner;

import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(
        snippets = CucumberOptions.SnippetType.CAMELCASE,
        publish = false,
        stepNotifications = false,
        useFileNameCompatibleName = false,
        features = "src/test/resources/features",
        glue = "apislistforpractice",
        plugin = {
                "pretty",
                "junit:target/surefire-reports/result.xml",
                "html:target/cucumber-reports"
        },
        tags = "@Apis and @CT005"
)
public class Runner {
}

