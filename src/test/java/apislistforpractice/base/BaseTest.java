package apislistforpractice.base;

import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;

import static io.restassured.RestAssured.given;

public class BaseTest {

    static {
        RestAssured.baseURI = "https://www.automationexercise.com";
    }

    protected RequestSpecification createRequest() {
        return given()
                .header("Content-Type", "application/json")
                .header("Accept", "application/json");
    }

    protected Response getRequest(String endpoint) {
        return createRequest()
                .when()
                .get(endpoint)
                .then()
                .extract()
                .response();
    }

    protected Response postRequest(String endpoint, Object body) {
        return createRequest()
                .body(body)
                .when()
                .post(endpoint)
                .then()
                .extract()
                .response();
    }

    protected Response putRequest(String endpoint, Object body) {
        return createRequest()
                .body(body)
                .when()
                .put(endpoint)
                .then()
                .extract()
                .response();
    }

    protected Response putRequestNoBody(String endpoint) {
        return createRequest()
                .when()
                .put(endpoint)
                .then()
                .extract()
                .response();
    }

    protected Response deleteRequest(String endpoint) {
        return createRequest()
                .when()
                .delete(endpoint)
                .then()
                .extract()
                .response();
    }

    protected Response postRequestNoBody(String endpoint) {
        return given()
                .contentType(ContentType.JSON)
                .when()
                .post(endpoint)
                .then()
                .extract()
                .response();
    }

    protected String getJsonValue(Response response, String jsonPath) {
        return response.jsonPath().getString(jsonPath);
    }

    protected int getStatusCode(Response response) {
        return response.getStatusCode();
    }

}
