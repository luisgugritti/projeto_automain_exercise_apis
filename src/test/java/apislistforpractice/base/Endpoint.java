package apislistforpractice.base;

public class Endpoint {

    public final static String endpointProductList = "/api/productsList";
    public final static String endpointBrandsList = "/api/brandsList";
    public final static String endpointSearchProduct = "/api/searchProduct";
    public final static String endpointVerifyLogin = "/api/verifyLogin";

}
