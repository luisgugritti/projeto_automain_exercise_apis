@Apis
  Feature: APIs - List For Practice

    Background: Realizar Testes nas APIs - List For Practice

      @CT001
      Scenario: API 1: GET All Products List
        Given usar o endpoint Get All Products List
        Then validar API GET com status code e response da Api 1

      @CT002
      Scenario: API 2: POST To All Products List
        Given usar o endpoint POST To All Products List
        Then validar API POST sem Body com status code e response da Api 2

      @CT003
      Scenario: API 3: Get All Brands List
        Given usar o endpoint Get All Brands List
        Then validar API GET com status code e response da Api 3

      @CT004
      Scenario: API 4: PUT To All Brands List
        Given usar o endpoint PUT To All Brands List
        Then validar API PUT sem Body com status code e response da Api 4

      @CT005
      Scenario: API 5: DELETE To Verify Login
        Given usar o endpoint DELETE To Verify Login
        Then validar API DELETE com status code e response da Api 7