# Projeto Automain Exercise APIs 🚀

![Java](https://img.shields.io/badge/Java-11-orange)
![Maven](https://img.shields.io/badge/Maven-3.8.1-blue)
![JUnit](https://img.shields.io/badge/JUnit-4.13.2-green)
![Cucumber](https://img.shields.io/badge/Cucumber-7.11.0-brightgreen)
![Rest-Assured](https://img.shields.io/badge/Rest--Assured-5.4.0-yellow)

## Descrição 📖

Este projeto é um exemplo de automação de testes de APIs utilizando **Rest-Assured**, **Cucumber** e **JUnit**. Ele demonstra como configurar um ambiente de testes eficiente e robusto para APIs.

## Estrutura do Projeto 🗂️

```bash
projeto_automain_exercise_apis
├── src
│   ├── main
│   │   └── java
│   └── test
│       └── java
├── pom.xml
└── README.md
```

## Configurações do Maven 🛠️

```xml
<project xmlns="http://maven.apache.org/POM/4.0.0"
         xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
    <modelVersion>4.0.0</modelVersion>

    <groupId>org.example</groupId>
    <artifactId>projeto_automain_exercise_apis</artifactId>
    <version>1.0-SNAPSHOT</version>

    <properties>
        <maven.compiler.source>11</maven.compiler.source>
        <maven.compiler.target>11</maven.compiler.target>
        <project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
    </properties>

    <dependencies>
        <dependency>
            <groupId>io.rest-assured</groupId>
            <artifactId>rest-assured</artifactId>
            <version>5.4.0</version>
            <scope>test</scope>
        </dependency>
        <dependency>
            <groupId>io.cucumber</groupId>
            <artifactId>cucumber-java</artifactId>
            <version>7.11.0</version>
        </dependency>
        <dependency>
            <groupId>io.cucumber</groupId>
            <artifactId>cucumber-junit</artifactId>
            <version>7.11.0</version>
        </dependency>
        <dependency>
            <groupId>junit</groupId>
            <artifactId>junit</artifactId>
            <version>4.13.2</version>
            <scope>test</scope>
        </dependency>
    </dependencies>
</project>
```

## Instalação 📦

Para instalar as dependências do projeto, certifique-se de ter o Maven instalado e execute o seguinte comando na raiz do projeto:

```sh
mvn clean install
```

## Execução dos Testes ✅

Para executar os testes automatizados, utilize o seguinte comando:

```sh
mvn test
```

## Tecnologias Utilizadas 🛠️

- [Java 11](https://www.oracle.com/java/technologies/javase-jdk11-downloads.html)
- [Maven 3.8.1](https://maven.apache.org/download.cgi)
- [Rest-Assured 5.4.0](https://rest-assured.io/)
- [Cucumber 7.11.0](https://cucumber.io/)
- [JUnit 4.13.2](https://junit.org/junit4/)

## Contribuições 🤝

Contribuições são bem-vindas! Sinta-se à vontade para abrir issues e pull requests.
